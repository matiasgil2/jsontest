﻿using System;
using System.Collections.Generic;
using System.Linq;
using Functional.Maybe;
using MatiasGil.SimpleGrid.Core.Actions;
using NSubstitute;
using NUnit.Framework;
using UniRx;

namespace MatiasGil.SimpleGrid.Presentation
{
    public class SimpleGridPresenterTest
    {
        private SimpleGridView view;
        private RetrieveGridAction retrieveGridAction;

        [SetUp]
        public void SetUp()
        {
            view = Substitute.For<SimpleGridView>();
            retrieveGridAction = Substitute.For<RetrieveGridAction>();
        
            var simpleGridPresenter = new SimpleGridPresenter(view,
                retrieveGridAction);
        
            view.ClearReceivedCalls();
        }

        [Test]
        public void when_updating_grid_clear_it_before_presenting()
        {
            GivenViewIsOpened();
            GivenRetrieveGridReturnsAGrid();
            GivenViewIsAlreadyPresented();

            WhenViewIsUpdated();
        
            ThenClearTheViewBeforePresentingTheGrid();
        }

        [Test]
        public void when_updating_grid_present_title()
        {
            GivenViewIsOpened();
            GivenRetrieveGridReturnsAGrid();
            GivenViewIsAlreadyPresented();

            WhenViewIsUpdated();

            ThenUpdateTitleInView();
        }

        [Test]
        public void when_updating_grid_present_headers()
        {
            GivenViewIsOpened();
            GivenRetrieveGridReturnsAGrid();
            GivenViewIsAlreadyPresented();

            WhenViewIsUpdated();

            ThenUpdateHeadersInView();
        }

        [Test]
        public void when_data_update_is_received_update_grid_content()
        {
            GivenViewIsOpened();
            GivenRetrieveGridReturnsAGrid();
        
            WhenViewIsUpdated();

            ThenUpdateGridContent();
        }

        private void GivenViewIsOpened()
        {
            view.OnViewEnabled += Raise.Event<Action>();
        }

        private void GivenRetrieveGridReturnsAGrid()
        {
            retrieveGridAction.Execute().Returns(
                Observable.Return(
                    GridMother.GetDefaultGrid().ToMaybe()));
        }

        private void GivenViewIsAlreadyPresented()
        {
            view.ClearReceivedCalls();
        }

        private void WhenViewIsUpdated()
        {
            view.OnUpdateClicked += Raise.Event<Action>();
        }

        private void ThenUpdateTitleInView()
        {
            view.Received(1).ShowTitle(Arg.Is(GridMother.DEFAULT_TITLE));
        }

        private void ThenUpdateHeadersInView()
        {
            var defaultHeaders = GridMother.DEFAULT_HEADERS;
            view.Received().ShowHeaders(
                Arg.Is<IEnumerable<string>>(headers => 
                    ContainSameElements(headers, defaultHeaders)));
        }

        private void ThenUpdateGridContent()
        {
            var defaultElements = GridMother.DEFAULT_ELEMENTS;
            var defaultHeaders = GridMother.DEFAULT_HEADERS;
        
            defaultElements.ForEach(defaultElementContent =>
            {
                view.Received(1).ShowNewElement(
                    Arg.Is<IDictionary<string, string>>(elementContent => 
                        ContainSameElements(elementContent, defaultElementContent)),
                    Arg.Is<IEnumerable<string>>(headers => 
                        ContainSameElements(headers, defaultHeaders)));
            });
        }

        private void ThenClearTheViewBeforePresentingTheGrid()
        {
            view.Received(1).Clear();
        
            var first = view.ReceivedCalls().First();
            Assert.IsTrue(first.GetMethodInfo().Name.Equals("Clear"));
        }

        private static bool ContainSameElements(IEnumerable<string> enumA, IEnumerable<string> enumB)
        {
            return enumA.Any(enumB.Contains) && enumB.Any(enumA.Contains);
        }
    
        private static bool ContainSameElements(IDictionary<string, string> dictA, IDictionary<string, string> dictB)
        {
            return dictA.Any(dictB.Contains) && dictB.Any(dictA.Contains);
        }
    }
}
