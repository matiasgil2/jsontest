﻿using System.Collections.Generic;
using MatiasGil.SimpleGrid.Core.Domain;
using MatiasGil.SimpleGrid.Infrastructure.Gateways;

namespace MatiasGil.SimpleGrid
{
    public static class GridMother
    {
        public const string DEFAULT_TITLE = "Lorem ipsum";

        public static List<string> DEFAULT_HEADERS = new List<string>
        {
            "A",
            "B",
            "C"
        };
        
        public static List<Dictionary<string, string>> DEFAULT_ELEMENTS = new List<Dictionary<string, string>>
        {
            new Dictionary<string, string>()
            {
                {"A", "Data1"},
                {"B", "Data1"},
                {"C", "Data1"}  
            },
            new Dictionary<string, string>()
            {
                {"F", "Data2"},
                {"B", "Data2"},
                {"C", "Data2"}  
            },
            new Dictionary<string, string>()
            {
                {"A", "Data3"},
                {"B", "Data3"},
                {"C", "Data3"}  
            }
        };
        
        public static Grid GetDefaultGrid()
        {
            return new Grid(
                DEFAULT_TITLE,
                DEFAULT_HEADERS,
                DEFAULT_ELEMENTS);
        }

        public static GridDTO GetDefaultGridDTO()
        {
            return new GridDTO(
                DEFAULT_TITLE,
                DEFAULT_HEADERS,
                DEFAULT_ELEMENTS);
        }
    }
}