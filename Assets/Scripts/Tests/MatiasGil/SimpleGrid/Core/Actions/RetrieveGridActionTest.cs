﻿using MatiasGil.SimpleGrid.Core.Domain;
using MatiasGil.SimpleGrid.Infrastructure.Gateways;
using NSubstitute;
using NUnit.Framework;
using UniRx;

namespace MatiasGil.SimpleGrid.Core.Actions
{
    public class RetrieveGridActionTest
    {
        private GridRepository repository;
        private DataGateway dataGateway;

        private RetrieveGridAction action;

        private GridDTO gridData;
        
        [SetUp]
        public void SetUp()
        {
            repository = Substitute.For<GridRepository>();
            dataGateway = Substitute.For<DataGateway>();
            
            action = new RetrieveGridAction(repository, dataGateway);
        }

        [Test]
        public void when_executing_action_data_should_be_loaded_from_gateway()
        {
            GivenDataIsReadyToBeLoadedFromGateway();

            WhenActionIsCalled();

            ThenDataShouldBeRetrievedFromGateway();
        }

        [Test]
        public void when_executing_action_data_should_be_saved_on_repo_after_retrieving_from_gateway()
        {
            GivenDataIsReadyToBeLoadedFromGateway();

            WhenActionIsCalled();

            TheDataShouldBeSavedInRepo();
        }

        private void GivenDataIsReadyToBeLoadedFromGateway()
        {
            gridData = GridMother.GetDefaultGridDTO();
            dataGateway.Load<GridDTO>().Returns(Observable.Return(gridData));
        }

        private void WhenActionIsCalled()
        {
            action.Execute().Subscribe();
        }

        private void ThenDataShouldBeRetrievedFromGateway()
        {
            dataGateway.Received(1).Load<GridDTO>();
        }

        private void TheDataShouldBeSavedInRepo()
        {
            repository.Received(1).Save(Arg.Is<Grid>(it => 
                it.Title.Equals(GridMother.DEFAULT_TITLE)));
        }
    }
}