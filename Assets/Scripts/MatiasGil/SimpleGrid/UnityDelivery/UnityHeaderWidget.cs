﻿using TMPro;
using UnityEngine;

namespace MatiasGil.SimpleGrid.UnityDelivery
{
    public class UnityHeaderWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI headerText;

        public void SetHeader(string text)
        {
            headerText.text = text;
        }
    }
}