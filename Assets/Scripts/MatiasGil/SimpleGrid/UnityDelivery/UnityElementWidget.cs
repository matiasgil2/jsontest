﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MatiasGil.SimpleGrid.UnityDelivery
{
    public class UnityElementWidget : MonoBehaviour
    {
        [SerializeField] private GameObject dataContainer;
        [SerializeField] private Image background;

        [SerializeField] private GameObject elementDataPrefab;

        public void SetData(List<string> data)
        {
            data.ForEach(InstantiateDataElement);
        }

        private void InstantiateDataElement(string dataText)
        {
            var elementData = Instantiate(elementDataPrefab, dataContainer.transform)
                .GetComponent<UnityElementDataWidget>();
            
            elementData.SetText(dataText);
        }

        public void SetBackgroundColor(Color backGroundColor)
        {
            background.color = backGroundColor;
        }
    }
}