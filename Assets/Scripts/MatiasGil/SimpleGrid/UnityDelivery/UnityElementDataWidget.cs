﻿using TMPro;
using UnityEngine;

namespace MatiasGil.SimpleGrid.UnityDelivery
{
    public class UnityElementDataWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI dataText;

        public void SetText(string text)
        {
            dataText.text = text;
        }
    }
}