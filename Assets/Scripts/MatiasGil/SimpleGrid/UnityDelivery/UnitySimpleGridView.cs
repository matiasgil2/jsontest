﻿using System;
using System.Collections.Generic;
using System.Linq;
using MatiasGil.SimpleGrid.Infrastructure.Providers;
using MatiasGil.SimpleGrid.Presentation;
using TMPro;
using UnityEngine;

namespace MatiasGil.SimpleGrid.UnityDelivery
{
    public class UnitySimpleGridView : MonoBehaviour, SimpleGridView
    {
        public event Action OnViewEnabled = () => { };
        public event Action OnUpdateClicked = () => { };

        [SerializeField] private TextMeshProUGUI title;
        [SerializeField] private GameObject headerContainer;
        [SerializeField] private GameObject elementsContainer;
        
        [SerializeField] private GameObject headerPrefab;
        [SerializeField] private GameObject elementPrefab;

        [SerializeField] private Color elementColorA;
        [SerializeField] private Color elementColorB;
        
        private List<GameObject> elements = new List<GameObject>();
        private List<GameObject> headers = new List<GameObject>();
        
        private const long UPDATE_THRESHOLD_MILLISECONDS = 500;
        private DateTime lastTimeUpdated;
        
        void Awake()
        {
            PresenterProvider.For(this);
        }

        void OnEnable()
        {
            OnViewEnabled();
        }

        private void Update()
        {
            ValidateUpdateInput();
        }

        public void ShowTitle(string titleText)
        {
            title.text = titleText;
        }

        public void ShowHeaders(IEnumerable<string> headers)
        {
            headers.ToList().ForEach(headerText => InstantiateNewHeader(headerText));
        }

        public void ShowNewElement(IDictionary<string, string> elementContent, IEnumerable<string> headers)
        {
            var dataList = OrderDataBasedOnHeaders(elementContent, headers);
            InstantiateNewElement(dataList);
        }

        public void Clear()
        {
            ClearElements();
            ClearHeaders();
        }

        private void InstantiateNewHeader(string headerText)
        {
            var headerView =  Instantiate(headerPrefab, headerContainer.transform).GetComponent<UnityHeaderWidget>();
            headerView.SetHeader(headerText);
            headers.Add(headerView.gameObject);
        }

        private void InstantiateNewElement(List<string> data)
        {
            var elementView = Instantiate(elementPrefab, elementsContainer.transform).GetComponent<UnityElementWidget>();

            elementView.SetData(data);
            
            SetElementBackgroundColor(elementView, elements.Count);

            elements.Add(elementView.gameObject);
        }

        private void SetElementBackgroundColor(UnityElementWidget elementView, int elementIndex)
        {
            Color backGroundColor;
            backGroundColor = elementIndex % 2 == 0 ? elementColorA : elementColorB;
            elementView.SetBackgroundColor(backGroundColor);
        }

        private static List<string> OrderDataBasedOnHeaders(IDictionary<string, string> elementContent, IEnumerable<string> headers)
        {
            var dataList = new List<string>();
            headers.ToList().ForEach(header =>
            {
                dataList.Add(elementContent.ContainsKey(header) ? elementContent[header] : string.Empty);
            });

            return dataList;
        }
        
        private void ValidateUpdateInput()
        {
            if (!Input.GetKeyDown(KeyCode.Space) ||
                !((DateTime.Now - lastTimeUpdated).TotalMilliseconds >= UPDATE_THRESHOLD_MILLISECONDS)) return;
            
            OnUpdateClicked();
            lastTimeUpdated = DateTime.Now;
        }

        private void ClearElements()
        {
            elements.ForEach(Destroy);
            elements.Clear();
        }

        private void ClearHeaders()
        {
            headers.ForEach(Destroy);
            headers.Clear();
        }
    }
}