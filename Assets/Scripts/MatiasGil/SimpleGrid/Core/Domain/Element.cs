﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MatiasGil.SimpleGrid.Core.Domain
{
    public class Element
    {
        private Dictionary<string, string> content;
        
        public Element(Dictionary<string, string> content)
        {
            this.content = content;
        }
        
        public ReadOnlyDictionary<string, string> GetContent()
        {
            return new ReadOnlyDictionary<string, string>(content);
        }
    }
}