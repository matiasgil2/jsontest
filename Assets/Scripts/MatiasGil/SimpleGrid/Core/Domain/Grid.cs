﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MatiasGil.SimpleGrid.Core.Domain
{
    public class Grid
    {
        public string Title { get; }
        private List<string> headers;
        private List<Element> elements;
        
        public Grid(string title, 
            IEnumerable<string> headers,
            IEnumerable<IDictionary<string, string>> elements)
        {
            Title = title;
            this.headers = headers.ToList();
            this.elements = elements
                .Select(content => new Dictionary<string, string>(content))
                .Select(newContentDictionary => new Element(newContentDictionary))
                .ToList();
        }

        public ReadOnlyCollection<string> GetHeaders()
        {
            return new ReadOnlyCollection<string>(headers);
        }

        public ReadOnlyCollection<Element> GetElements()
        {
            return new ReadOnlyCollection<Element>(elements);
        }
    }
}