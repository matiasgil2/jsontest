﻿using System;

namespace MatiasGil.SimpleGrid.Core.Actions
{
    public interface DomainAction<T>
    {
        IObservable<T> Execute();
    }
}