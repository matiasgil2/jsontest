﻿using System;
using Functional.Maybe;
using MatiasGil.SimpleGrid.Core.Domain;
using MatiasGil.SimpleGrid.Infrastructure.Gateways;
using UniRx;

namespace MatiasGil.SimpleGrid.Core.Actions
{
    public class RetrieveGridAction : DomainAction<Maybe<Grid>>
    {
        private readonly GridRepository gridRepository;
        private readonly DataGateway jsonGridDataGateway;

        public RetrieveGridAction()
        {
        }

        public RetrieveGridAction(GridRepository gridRepository,
            DataGateway jsonGridDataGateway)
        {
            this.gridRepository = gridRepository;
            this.jsonGridDataGateway = jsonGridDataGateway;
        }

        public virtual IObservable<Maybe<Grid>> Execute()
        {
            return jsonGridDataGateway.Load<GridDTO>()
                .Select(dto => dto.ToGrid())
                .SelectMany(gridRepository.Save);
        }
    }
}
