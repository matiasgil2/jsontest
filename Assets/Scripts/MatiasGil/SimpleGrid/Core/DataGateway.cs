﻿using System;

namespace MatiasGil.SimpleGrid.Core
{
    public interface DataGateway
    {
        IObservable<T> Load<T>();
    }
}