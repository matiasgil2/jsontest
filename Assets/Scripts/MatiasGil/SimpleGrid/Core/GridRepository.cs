﻿using System;
using Functional.Maybe;
using MatiasGil.SimpleGrid.Core.Domain;

namespace MatiasGil.SimpleGrid.Core
{
    public interface GridRepository
    {
        IObservable<Maybe<Grid>> Save(Grid newGrid);
        IObservable<Maybe<Grid>> Retrieve();
    }
}