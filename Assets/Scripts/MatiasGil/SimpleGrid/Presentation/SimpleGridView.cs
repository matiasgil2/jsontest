﻿using System;
using System.Collections.Generic;

namespace MatiasGil.SimpleGrid.Presentation
{
    public interface SimpleGridView
    {
        event Action OnViewEnabled;
        event Action OnUpdateClicked;
        
        void ShowTitle(string titleText);
        void ShowHeaders(IEnumerable<string> headers);
        void ShowNewElement(IDictionary<string, string> elementContent, IEnumerable<string> headers);
        void Clear();
    }
}
