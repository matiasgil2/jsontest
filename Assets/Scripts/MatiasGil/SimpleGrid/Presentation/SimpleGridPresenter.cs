﻿using System.Collections.Generic;
using System.Linq;
using Functional.Maybe;
using MatiasGil.SimpleGrid.Core.Actions;
using MatiasGil.SimpleGrid.Core.Domain;
using UniRx;

namespace MatiasGil.SimpleGrid.Presentation
{
    public class SimpleGridPresenter
    {
        private readonly SimpleGridView view;
        private readonly RetrieveGridAction retrieveGridAction;

        public SimpleGridPresenter(SimpleGridView view, RetrieveGridAction retrieveGridAction)
        {
            this.view = view;
            this.retrieveGridAction = retrieveGridAction;

            view.OnViewEnabled += ViewEnabled;
            view.OnUpdateClicked += UpdateGrid;
        }

        private void ViewEnabled()
        {
            UpdateGrid();
        }

        private void UpdateGrid()
        {
            retrieveGridAction.Execute()
                .Do(maybeGrid => maybeGrid.Do(PresentGrid))
                .Subscribe();
        }

        private void PresentGrid(Grid grid)
        {
            ClearView();
        
            ShowTitle(grid.Title);
            ShowHeaders(grid.GetHeaders());
            ShowElements(grid.GetElements(), grid.GetHeaders());
        }

        private void ClearView()
        {
            view.Clear();
        }

        private void ShowTitle(string title)
        {
            view.ShowTitle(title);
        }

        private void ShowHeaders(IEnumerable<string> headers)
        {
            view.ShowHeaders(headers);
        }

        private void ShowElements(IEnumerable<Element> elements, IEnumerable<string> headers)
        {
            elements.ToList()
                .ForEach(element => view.ShowNewElement(element.GetContent(), headers.ToList()));
        }
    }
}
