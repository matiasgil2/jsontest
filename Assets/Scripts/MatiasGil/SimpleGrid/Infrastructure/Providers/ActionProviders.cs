﻿using MatiasGil.Shared.Tools;
using MatiasGil.SimpleGrid.Core.Actions;

namespace MatiasGil.SimpleGrid.Infrastructure.Providers
{
    public static class ActionProviders
    {
        public static RetrieveGridAction ProvideRetrieveGridAction()
        {
            return DependencyProvider.GetOrInstanciate<RetrieveGridAction>(() =>
                new RetrieveGridAction(RepositoryProvider.ProvideGridRepository(),
                    GatewayProvider.ProvideGridDataGateway()));
        }
    }
}