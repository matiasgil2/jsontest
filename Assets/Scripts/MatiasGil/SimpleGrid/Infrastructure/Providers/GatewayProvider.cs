﻿using MatiasGil.Shared.Tools;
using MatiasGil.SimpleGrid.Core;
using MatiasGil.SimpleGrid.Infrastructure.Gateways;

namespace MatiasGil.SimpleGrid.Infrastructure.Providers
{
    public static class GatewayProvider
    {
        public static DataGateway ProvideGridDataGateway()
        {
            return DependencyProvider.GetOrInstanciate<JsonGridDataGateway>(() => 
                new JsonGridDataGateway());
        }
    }
}