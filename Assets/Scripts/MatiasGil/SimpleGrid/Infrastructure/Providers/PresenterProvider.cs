﻿using MatiasGil.SimpleGrid.Presentation;

namespace MatiasGil.SimpleGrid.Infrastructure.Providers
{
    public static class PresenterProvider
    {
        public static SimpleGridPresenter For(SimpleGridView view)
        {
            return new SimpleGridPresenter(view,
                ActionProviders.ProvideRetrieveGridAction());
        }
    }
}