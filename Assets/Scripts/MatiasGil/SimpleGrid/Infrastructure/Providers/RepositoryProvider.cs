﻿using MatiasGil.Shared.Tools;
using MatiasGil.SimpleGrid.Core;
using MatiasGil.SimpleGrid.Infrastructure.Repositories;

namespace MatiasGil.SimpleGrid.Infrastructure.Providers
{
    public static class RepositoryProvider
    {
        public static GridRepository ProvideGridRepository()
        {
            return DependencyProvider.GetOrInstanciate<GridRepository>(() =>
                new InMemoryGridRepository());
        }
    }
}