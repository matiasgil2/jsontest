﻿using System;
using System.IO;
using MatiasGil.SimpleGrid.Core;
using Newtonsoft.Json;
using UniRx;
using UnityEngine;

namespace MatiasGil.SimpleGrid.Infrastructure.Gateways
{
    public class JsonGridDataGateway : DataGateway
    {
        public const string JSON_FILE_NAME = "/gridData.json";
        
        public IObservable<GridDTO> Load<GridDTO>()
        {
            return Observable.ReturnUnit()
                .Select(_ => Application.streamingAssetsPath + JSON_FILE_NAME)
                .ObserveOn(Scheduler.ThreadPool)
                .Select(ReadJsonFromDisk<GridDTO>)
                .ObserveOnMainThread();
        }

        private static T ReadJsonFromDisk<T>(string path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    return JsonConvert.DeserializeObject<T>(reader.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}