﻿using System.Collections.Generic;
using System.Linq;
using MatiasGil.SimpleGrid.Core.Domain;
using Newtonsoft.Json;

namespace MatiasGil.SimpleGrid.Infrastructure.Gateways
{
    public class GridDTO
    {
        [JsonProperty("Title")] private string title;
        [JsonProperty("ColumnHeaders")] private List<string> headers;
        [JsonProperty("Data")] private List<Dictionary<string, string>> elements;

        public GridDTO(string title,
            List<string> headers,
            List<Dictionary<string, string>> elements)
        {
            this.title = title;
            this.headers = headers;
            this.elements = elements;
        }

        public Grid ToGrid()
        {
            return new Grid(title, headers.ToList(), elements);
        }
    }
}