﻿using System;
using Functional.Maybe;
using MatiasGil.SimpleGrid.Core;
using MatiasGil.SimpleGrid.Core.Domain;
using UniRx;

namespace MatiasGil.SimpleGrid.Infrastructure.Repositories
{
    public class InMemoryGridRepository : GridRepository
    {
        Maybe<Grid> grid = Maybe<Grid>.Nothing;

        public InMemoryGridRepository()
        {
        }

        public IObservable<Maybe<Grid>> Save(Grid newGrid)
        {
            grid = newGrid.ToMaybe();
            return Retrieve();
        }

        public IObservable<Maybe<Grid>> Retrieve()
        {
            return Observable.Return(grid);
        }
    }
}