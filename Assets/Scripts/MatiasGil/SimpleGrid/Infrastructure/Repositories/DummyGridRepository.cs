﻿using System;
using System.Collections.Generic;
using Functional.Maybe;
using MatiasGil.SimpleGrid.Core;
using MatiasGil.SimpleGrid.Core.Actions;
using MatiasGil.SimpleGrid.Core.Domain;
using UniRx;

namespace MatiasGil.SimpleGrid.Infrastructure.Repositories
{
    public class DummyGridRepository : GridRepository
    {
        Maybe<Grid> maybeGrid = Maybe<Grid>.Nothing;

        public const string DEFAULT_TITLE = "Lorem ipsum";

        public static List<string> DEFAULT_HEADERS = new List<string>
        {
            "F",
            "C",
            "B",
            "A"
        };
        
        public static List<Dictionary<string, string>> DEFAULT_ELEMENTS = new List<Dictionary<string, string>>
        {
            new Dictionary<string, string>()
            {
                {"A", "Data1-A"},
                {"C", "Data1-C"} ,
                {"B", "Data1-B"}
                 
            },
            new Dictionary<string, string>()
            {
                {"F", "Data2-F"},
                {"B", "Data2-B"},
                {"C", "Data2-C"}  
            },
            new Dictionary<string, string>()
            {
                {"A", "Data3-A"},
                {"B", "Data3-B"}
            },
            new Dictionary<string, string>()
            {
                {"B", "Data4-B"},
                {"C", "Data4-C"} ,
                {"D", "Data4-D"}
                 
            },
            new Dictionary<string, string>()
            {
                {"F", "Data5-F"},
                {"A", "Data5-A"},
                {"C", "Data5-C"}
            },
            new Dictionary<string, string>()
            {
                {"A", "Data6-A"},
                {"C", "Data6-C"}
            }
        };
        
        public DummyGridRepository()
        {
            var grid = new Grid(
                DEFAULT_TITLE,
                DEFAULT_HEADERS,
                DEFAULT_ELEMENTS);
            
            maybeGrid = grid.ToMaybe();
        }

        public IObservable<Maybe<Grid>> Save(Grid grid)
        {
            maybeGrid = grid.ToMaybe();
            return Retrieve();
        }

        public IObservable<Maybe<Grid>> Retrieve()
        {
            return Observable.Return(maybeGrid);
        }
    }
}