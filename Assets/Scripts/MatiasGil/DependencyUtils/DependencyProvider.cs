﻿using System;
using System.Collections.Generic;

namespace MatiasGil.Shared.Tools
{
    public static class DependencyProvider
    {
        static readonly Dictionary<string, object> instances = new Dictionary<string, object>();
 
        static DependencyProvider()
        {
        }
 
        public static T Get<T>(string id = null)
        {
            var type = typeof(T).FullName;
            if (id != null)
                type = id;
            object singletonObj;
 
            if (instances.TryGetValue(type, out singletonObj))
                return (T) singletonObj;
 
            return default(T);
        }
 
        public static void Set<T>(object singletonObj, string id = null)
        {
            var type = typeof(T).FullName;
            if (id != null)
                type = id;
            instances[type] = singletonObj;
        }
 
        public static T GetOrInstanciate<T>(Func<object> instantiator, string id = null)
        {
            var type = typeof(T).FullName;
            if (id != null)
                type = id;
 
            object singletonObj = Get<T>(type);
            object nullValue = default(T);
 
            if (singletonObj != nullValue)
                return (T) singletonObj;
            return Instanciate<T>(instantiator, type);
        }
 
        public static T Instanciate<T>(Func<object> instanciator, string id)
        {
            var singletonObj = instanciator();
 
            instances[id] = singletonObj;
            return (T) singletonObj;
        }
    }
}