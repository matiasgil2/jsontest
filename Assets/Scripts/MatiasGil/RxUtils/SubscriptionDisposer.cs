using System;
using System.Collections.Generic;

namespace MatiasGil.RxUtils
{
    public class SubscriptionDisposer : IDisposable
    {
        List<IDisposable> disposables = new List<IDisposable>();

        public static SubscriptionDisposer Create()
        {
            return new SubscriptionDisposer();
        }

        public SubscriptionDisposer Add(IDisposable disposable)
        {
            disposables.Add(disposable);
            return this;
        }

        public void Dispose()
        {
            foreach (var disposable in disposables){
                if(disposable != null)
                    disposable.Dispose();
            }

            disposables.Clear();
        }
    }
}